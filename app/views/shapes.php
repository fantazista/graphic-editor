<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <title>Graph Editor</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Graph Editor</a>
        </div>
    </div>
</nav>

<div class="container">
    <br>
    <br>
    <br>
    <br>

    <form method="POST" action="/index.php">
        <div class="form-group">
            <label for="exampleTextarea">Enter shapes config. Example - [{"type":"circle","params":[]},{"type":"square","params":[]}]</label>
            <textarea name="shapes" class="form-control" id="exampleTextarea" rows="3" placeholder='[{"type":"circle","params":[]},{"type":"square","params":[]}]'></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Draw</button>
    </form>

    <br>
    <br>

    <div>
        <?php foreach ($shapes as $shape): ?>
            <div>
                <img src="<?php echo $shape['img'] ?>"/>
            </div>
            params: <?php echo var_dump($shape['params']); ?><br/>
        <?php endforeach; ?>
    </div>
</div>

</body>
</html>
