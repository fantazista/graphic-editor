<?php

namespace App\Shapes;

interface DrawableInterface
{
    /**
     * @return mixed
     */
    public function draw();
}
