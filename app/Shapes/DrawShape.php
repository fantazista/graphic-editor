<?php

namespace App\Shapes;

class DrawShape
{
    /**
     * @var DrawableInterface
     */
    private $shapes;

    /**
     * DrawShape constructor.
     * @param DrawableInterface[] $shape
     */
    public function __construct(array $shape)
    {
        $this->shapes = $shape;
    }

    /**
     * @return array
     */
    public function draw()
    {
        $result = [];
        foreach ($this->shapes as $shape) {
            $result[] = $shape->draw();
        }

        return $result;
    }
}
