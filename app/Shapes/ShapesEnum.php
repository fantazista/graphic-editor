<?php

namespace App\Shapes;

class ShapesEnum
{
    const CIRCLE = 'circle';
    const SQUARE = 'square';
}
