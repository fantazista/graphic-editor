<?php

namespace App\Shapes;

class ShapeValidation implements ValidationInterface
{
    /**
     * @param mixed $params
     * @return bool
     */
    public function check($params)
    {
        if ($result = json_decode($params, true)) {
            if (is_array($result)) {
                return $result;
            }
        }

        return [];
    }
}
