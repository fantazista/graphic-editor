<?php

namespace App\Shapes;

class Circle extends AbstractShape implements DrawableInterface
{
    /**
     * Please, skip all calculations for shapes (mocks, dummy methods are OK here).
     * Architecture is more interesting for us.
     *
     * @return mixed
     */
    public function draw()
    {
        //mock
        return [
            'img' => '/img/circle.png',
            'params' => $this->getParams(),
        ];
    }
}
