<?php

namespace App\Shapes;

class AbstractShape
{
    /**
     * @var array
     */
    protected $params;

    /**
     * AbstractShape constructor.
     * @param [] $params
     */
    public function __construct($params = [])
    {
        $this->params = $params;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }
}
