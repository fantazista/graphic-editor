<?php

namespace App\Shapes\Factory;

use App\Shapes\AbstractShape;
use App\Shapes\Circle;
use App\Shapes\Factory\Exception\ShapeNotFoundException;
use App\Shapes\ShapesEnum;
use App\Shapes\Square;

class ShapeFactory
{
    /**
     * @param array $shapes
     * @return AbstractShape[]
     * @throws ShapeNotFoundException
     */
    public static function create(array $shapes)
    {
        $result = [];
        foreach ($shapes as $shape) {
            $result[] = self::getShape($shape);
        }

        return $result;
    }

    /**
     * @param array $shape
     * @return Circle|Square
     * @throws ShapeNotFoundException
     */
    public static function getShape(array $shape)
    {
        $type = $shape['type'] ?? '';
        $params = $shape['params'] ?? [];

        switch($type) {
            case ShapesEnum::CIRCLE:
                return new Circle($params);
            case ShapesEnum::SQUARE:
                return new Square($params);
            default:
                throw new ShapeNotFoundException('Shape ' . $shape . 'doesn\'t exist');
        }
    }
}
