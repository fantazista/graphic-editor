<?php

namespace App\Shapes;

interface ValidationInterface
{
    /**
     * @param mixed $params
     * @return mixed
     */
    public function check($params);
}
