<?php

namespace App\Shapes;

class Square extends AbstractShape implements DrawableInterface
{
    /**
     * Please, skip all calculations for shapes (mocks, dummy methods are OK here).
     * Architecture is more interesting for us.
     *
     * @return mixed
     */
    public function draw()
    {
        //mock
        return [
            'img' => '/img/square.jpeg',
            'params' => $this->getParams(),
        ];
    }
}
