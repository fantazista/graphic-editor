<?php

namespace App\Controllers;

use App\Shapes\DrawShape;
use App\Shapes\Factory\ShapeFactory;
use App\Shapes\ValidationInterface;
use Container;
use View;

class ShapesController
{
    /**
     * @var \Request
     */
    private $request;

    /**
     * @var ValidationInterface
     */
    private $validation;

    /**
     * ShapesController constructor.
     */
    public function __construct()
    {
        $this->request = Container::get('request');
        $this->validation = Container::get('shape-validation');
    }

    /**
     * @return string
     */
    public function drawAction()
    {
        if ($shapes = $this->validation->check($this->request->getPost('shapes'))) {
            $shapes = (new DrawShape(ShapeFactory::create($shapes)))->draw();
        }

        return View::render('shapes', ['shapes' => $shapes]);
    }
}
