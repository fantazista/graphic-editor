<?php

namespace App\Controllers;

use App\Shapes\DrawShape;
use App\Shapes\Factory\ShapeFactory;
use View;

class ConsoleController
{
    /**
     * @var array
     */
    private $data;

    /**
     * ShapesController constructor.
     * @param array $argv
     * @throws \Exception
     */
    public function __construct(array $argv)
    {
        $this->data = $argv;
        $this->validation = \Container::get('shape-validation');
    }

    /**
     * @return string
     */
    public function drawAction()
    {
        if ($shapes = $this->validation->check($this->data[1])) {
            $shapes = (new DrawShape(ShapeFactory::create($shapes)))->draw();
        }

        return View::render('shapes', ['shapes' => $shapes]);
    }

}
