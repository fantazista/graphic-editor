<?php

use App\Controllers\ConsoleController;
use App\Controllers\ShapesController;
use App\Shapes\ShapeValidation;

require 'bootstrap.php';

Container::bind('request', new Request());
Container::bind('shape-validation', new ShapeValidation());

if (PHP_SAPI === 'cli') {
    $controller = new ConsoleController($argv);
    $controller->drawAction();
} else {
    $controller = new ShapesController();
    $controller->drawAction();
}

