# README #

Test task

### description ###

Graphic Editor

Please prepare a service for a simple app called “Graphic Editor”. The service has to be able to draw geometric shapes (circle, square, rectangle, ellipse, etc). Add implementation for circle and square, but there should be possibility to add any other shape in the service fast, easy and with minimum code changes. Each shape might have various attributes: color, border size, etc.

The app usage:

It gets a multi-array to the input. Example:

```
$shapes = [
   ['type' => 'circle', 'params' => [...]],
   ['type' => 'circle', 'params' => [...]]
];
```

It makes all calculations and shows a result in any format (array of points, image(binary file) etc.).


Prepare an architecture for this service in PHP5/PHP7 with controller/cli endpoint and upload it to github.


* Please, skip all calculations for shapes (mocks, dummy methods are OK here). Architecture is more interesting for us.
* OOP design patterns are required.
* Don't use of any frameworks or third-party code.

### How do I get set up? ###

* git clone git@bitbucket.org:fantazista/graphic-editor.git
* setup virtual host
* composer install for test

cli call example - sh draw.sh '[{"type":"square","params":[]},{"type":"square","params":{"border":1}}]'
