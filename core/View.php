<?php

class View
{
    public static function render($name, $data = [])
    {
        extract($data);
        return require 'app/views/' . $name . '.php';
    }
}
