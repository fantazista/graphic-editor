<?php

class Request
{
    /**
     * @var array
     */
    private $post;

    /**
     * Request constructor.
     */
    public function __construct()
    {
        $this->post = $_POST;
    }

    /**
     * @param string $key
     * @return array
     */
    public function getPost($key)
    {
        return $this->post[$key] ?? null;
    }
}
