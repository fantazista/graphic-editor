<?php

namespace Tests;

use App\Shapes\DrawShape;
use App\Shapes\Factory\ShapeFactory;
use PHPUnit_Framework_TestCase;

class TestShape extends PHPUnit_Framework_TestCase
{
    public function testOneShape()
    {
        $shapes = [
            ['type' => 'square', 'params' => ['border' => 2, 'color' => 'red']],
        ];

        $result = (new DrawShape(ShapeFactory::create($shapes)))->draw();

        $this->assertEquals(1, count($result));
        $this->assertEquals(2, $result[0]['params']['border']);
        $this->assertSame('red', $result[0]['params']['color']);
        $this->assertSame('/img/square.jpeg', $result[0]['img']);
    }

    public function testBasicTest()
    {
        $shapes = [
            ['type' => 'square', 'params' => ['border' => 2]],
            ['type' => 'circle', 'params' => ['border' => 2]],
            ['type' => 'square', 'params' => ['border' => 2]],
        ];

        $result = (new DrawShape(ShapeFactory::create($shapes)))->draw();

        $this->assertEquals(3, count($result));
        $this->assertSame('/img/circle.png', $result[1]['img']);
    }
}
